package com.rozvi14.facialrecognition;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class ActivityMain extends AppCompatActivity {

    Button btn_inicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        btn_inicio = (Button) findViewById(R.id.btn_inicio);

    }

    public void accion_boton(View v){
        Intent intent = new Intent(this, FaceTrackerActivity.class);
        startActivity(intent);
    }
}
