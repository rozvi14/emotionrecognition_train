import os
import cv2
import numpy as np
import keras
from pandas import DataFrame, read_csv
from datetime import datetime

from keras.callbacks import CSVLogger, EarlyStopping
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize

import Modelos

# VARIABLE INICIALES
URL_IMAGES = ["./data/extended-cohn-kanade-images/cohn-kanade-haar"
    , "./data/jaffeimages/jaffe_haar2"
    , "./data/KDEF_and_AKDEF/KDEF_and_AKDEF/KDEFHaar/"
    , "./data/All_haar/"
              ,"./data/get-server/DATA_P1/"]
NAME_IMAGES = ['Cohn-Kanade'
    , 'JAFFE'
    , "KDEF"
    , "All"
               ,"Server"]

nro_model = 3
num_classes = 6
#model, NAME_MODEL = Modelos.generar_model(num_classes, nro_model)
model = load_model('./models/OWNRETRAIN_6emo_All_Max_E100_B16-model.h5')
NAME_MODEL = "OWNRETRAIN2"
MODEL = NAME_MODEL+"_6emo"
TYPE_NORM = ['Max'
    , 'L2']
PATH_CSV_RESULT = './resultados_time.csv'

batch_size_ini = 10
jump_batch = 10
epochs_ini = 5
jump_epoch = 5


columns = ['model', 'BD', 'Norm', 'Epochs', 'Batchs', 'Accu', 'Loss']


def leer_imagenes(url, type_norm):
    path = url
    X = []
    y = []
    lstDir = os.walk(path)
    i = 1
    for root, dirs, files in lstDir:
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
            if (extension == ".png"):
                ruta_img = root + '\\' + nombreFichero + extension
                label = int(nombreFichero[0:1])
                data = cv2.imread(ruta_img)
                if data.any() and label < 7 and label != 1:
                    data = cv2.resize(data, (48, 48))
                    data = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)
                    data = np.array(data, 'float32')
                    if type_norm == 0:
                        data /= 255
                    elif type_norm == 1:
                        data = normalize(data, norm='l2')

                    if label > 1:
                        label = label - 1

                    emotion = keras.utils.to_categorical(label, num_classes)

                    X.append(data)
                    y.append(emotion)
    X = np.array(X, 'float32')
    X = X.reshape(X.shape[0], X.shape[1], X.shape[2], 1)

    y = np.array(y)
    return X, y




def entrenar_model(X_train, y_train, X_test, y_test, epochs, batch_size, NOMBRE_MODEL, title=""):


    print(title + "Compilando modelo")
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])  # reporting the accuracy

    print(title + "Entrenando modelo")

    gen = ImageDataGenerator()
    train_generator = gen.flow(X_train, y_train, batch_size=batch_size)

    #CALLABACK DEL FIT
    early_stopper = EarlyStopping(monitor='val_loss', min_delta=0, patience=1, verbose=0, mode='auto')
    csv_logger = CSVLogger('./historys/' + NOMBRE_MODEL + '.csv')

    history = model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, callbacks=[csv_logger, early_stopper], validation_data=(X_test, y_test))

    model.save('./models/' + NOMBRE_MODEL + '-model.h5')

    score = model.evaluate(X_test, y_test)
    print('Test loss:', score[0])
    print('Test accuracy:', 100 * score[1])

    return model, history, score


if __name__ == '__main__':

    if os.path.isfile(PATH_CSV_RESULT):
        csvResult = read_csv(PATH_CSV_RESULT, index_col=0)
    else:
        csvResult = DataFrame(columns=columns)

    train_unit = True
    train_batch = False

    if train_unit:
        index_bd = 4
        index_norm = 0
        name_bd = NAME_IMAGES[index_bd]
        tipo_norm = TYPE_NORM[index_norm]
        epochs = 100
        batch_size = 6

        X, y = leer_imagenes(URL_IMAGES[index_bd], index_norm)
        validation_size = 0.20
        seed = datetime.now().year + datetime.now().month + datetime.now().day + datetime.now().hour + datetime.now().minute + datetime.now().microsecond
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=validation_size, random_state=seed)

        NOMBRE_MODEL = MODEL+"_" + name_bd + "_" + tipo_norm + "_E" + str(epochs) + "_B" + str(batch_size)

        model, history, score = entrenar_model(X_train, y_train, X_test, y_test, epochs, batch_size,
                                               NOMBRE_MODEL)
        csvResult = csvResult.append({
            'model': MODEL, 'BD': name_bd, 'Norm': tipo_norm, 'Epochs': epochs, 'Batchs': batch_size, 'Accu': score[1],
            'Loss': score[0]
        }, ignore_index=True)
        print("=========== GUARDANDO RESULTADOS ===============")
        csvResult.to_csv('./resultados_time.csv')


    if train_batch:
        result = []
        nro_bases = len(URL_IMAGES)
        for i in range(3, 4):
            print("================ INICIO " + str(i) + " =========================")
            name_bd = NAME_IMAGES[i]
            title = "[" + name_bd + "] - "
            print("[BASE DE DATOS]: " + name_bd)

            for j in range(0, len(TYPE_NORM)):
                print(title + "Lectura de imagenes")
                tipo_norm = TYPE_NORM[j]
                print(title + "Tipo de Norm.: " + tipo_norm)
                X, y = leer_imagenes(URL_IMAGES[i], j)
                print(title + "Datos leido: " + str(X.shape[0]))

                validation_size = 0.20
                seed = datetime.now().year + datetime.now().month + datetime.now().day + datetime.now().hour + datetime.now().minute + datetime.now().microsecond
                X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=validation_size, random_state=seed)
                print(title + "Nro. entrenamiento: " + str(X_train.shape[0]))
                print(title + "Nro. prueba: " + str(X_test.shape[0]))
                for k in range(0, 2):
                    epochs = epochs_ini + k * jump_epoch
                    for l in range(0, 10):
                        batch_size = batch_size_ini + l * jump_batch

                        print(title + "EPOCHS: " + str(epochs))
                        print(title + "BACTH: " + str(batch_size))
                        NOMBRE_MODEL = MODEL + "_" + name_bd + "_" + tipo_norm + "_E" + str(epochs) + "_B" + str(batch_size)

                        model, score, history = entrenar_model(X_train, y_train, X_test, y_test, epochs, batch_size,
                                                               NOMBRE_MODEL, title)

                        csvResult = csvResult.append({
                            'model': MODEL, 'BD': name_bd, 'Norm': tipo_norm, 'Epochs': epochs, 'Batchs': batch_size, 'Accu': score[1], 'Loss': score[0]
                        })


                        '''
                        plt.plot(history.history['acc'])
                        plt.title('Exactitud del Modelo')
                        plt.ylabel('Exactitud')
                        plt.xlabel('Iteración')
                        plt.legend(['Entranamiento', 'Prueba'], loc='upper left')
                        plt.savefig('./imagenes/entrenamiento/'+NOMBRE_MODEL+'_acc.png')
                        plt.show()
    
                        plt.plot(history.history['loss'])
                        plt.title('Pérdida del modelo')
                        plt.ylabel('Pérdida')
                        plt.xlabel('Iteración')
                        plt.legend(['Entranamiento', 'Prueba'], loc='upper left')
                        plt.savefig('./imagenes/entrenamiento/'+NOMBRE_MODEL+'_loss.png')
                        plt.show()
                        '''
            print("================ FIN " + str(i) + " =========================")

        print("=========== GUARDANDO RESULTADOS ===============")
        csvResult.to_csv('./resultados_time.csv')


