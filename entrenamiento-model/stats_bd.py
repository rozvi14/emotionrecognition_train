import os
import cv2
import numpy as np

URL_IMAGES = ["./data/extended-cohn-kanade-images/cohn-kanade-haar"
    , "./data/jaffeimages/jaffe_haar2"
    , "./data/KDEF_and_AKDEF/KDEF_and_AKDEF/KDEFHaar/"
    , "./data/All_haar/"]


emotions = ('Furia', 'Disgusto', 'Miedo', 'Felicidad', 'Tristeza', 'Sorpresa', 'Neutral', 'Desprecio')

if __name__ == '__main__':
    emotions = [0,0,0,0,0,0,0,0]
    index_bd = 3

    if index_bd<4:
        path = URL_IMAGES[index_bd]
        X = [[],[],[],[],[],[],[],[]]
        y = []
        lstDir = os.walk(path)
        i = 1
for root, dirs, files in lstDir:
    for fichero in files:
        (nombreFichero, extension) = os.path.splitext(fichero)
        if (extension == ".png"):
            ruta_img = root + '\\' + nombreFichero + extension
            label = int(nombreFichero[0:1])
            data = cv2.imread(ruta_img)
            if data.any():
                data = cv2.resize(data, (48, 48))
                data = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)
                #emotions[label] = emotions[label] + 1
                X[label].append(data)

    else:

        with open("./data/fer2013/fer2013.csv") as f:
            content = f.readlines()

        lines = np.array(content)

        num_of_instances = lines.size
        print("Nro de imagenes: ", num_of_instances)
        print("Tamaño de imagen: ", len(lines[1].split(",")[1].split(" ")))


        x_train, y_train, x_test, y_test = [], [], [], []


        for i in range(1, num_of_instances):
            try:
                label, img, usage = lines[i].split(",")

                val = img.split(" ")

                pixels = np.array(val, 'float32')
                label = int(label)
                if ('Training' in usage) or ('PublicTest' in usage):
                    emotions[label] = emotions[label] + 1

            except:
                print("", end="")
    print(emotions)