import os
import cv2
import glob

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')


if __name__ == '__main__':
    path_origin = './data/extended-cohn-kanade-images/'
    path_img = 'cohn-kanade-images/'
    path_save = 'cohn-kanade-haar/'
    path_label = 'Emotion/'

    lstFiles = []
    lstDir = os.walk(path_origin+path_img)

    nro_emociones = [0,0,0,0,0,0,0,0]

    print("================INICIO - LECTURA NEUTRAL ====================")
    for carpeta in os.listdir(path_origin+path_img):
        print("["+path_origin+path_img+"]")
        obten_neutral = True
        lstDirSujeto = []
        lstDirSujeto = os.walk(path_origin + path_img+carpeta)
        for root, dirs, files in lstDirSujeto:
            list_archivos = glob.glob(root + "/*.png")
            nro_archivos = len(list_archivos)
            if nro_archivos > 0:
                # EXTRAER NEUTRALES
                if obten_neutral:
                    for i in range(0, 3):
                        roi_gray = []
                        img = cv2.imread(list_archivos[i])
                        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
                        for (x, y, w, h) in faces:
                            roi_gray = img[y:y + h, x:x + w]
                        if roi_gray.any():
                            name_file = list_archivos[i][len(list_archivos[i]) - 21:len(list_archivos[i])]
                            resize = cv2.resize(roi_gray, (244, 244))
                            em = 6
                            cv2.imwrite(path_origin + path_save + str(em) + "_" + name_file, resize)
                            print("[" + root + "] - Neutral" + name_file)
                            nro_emociones[0] = nro_emociones[0] + 1
                    obten_neutral = False

    print("================INICIO - LECTURA OTROS ====================")
    for root, dirs, files in lstDir:
        list_archivos = glob.glob(root + "/*.png")
        nro_archivos = len(list_archivos)
        if nro_archivos > 0:
            print("[" + root + "]")
            path = str(root)
            subpath = path[len(path)-8: len(path)]

            #RECORTAR DEMAS EMOCIONES
            list_file_label = glob.glob(path_origin + path_label + subpath + "/*.txt")
            if len(list_file_label) > 0:
                f = open(list_file_label[0], 'r')
                txt = f.readline().replace(" ", "").replace("\n", "")
                label = int(float(txt))
                f.close()

                for i in range(nro_archivos-3, nro_archivos):
                    roi_gray = []
                    img = cv2.imread(list_archivos[i])
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
                    for (x, y, w, h) in faces:
                        roi_gray = img[y:y + h, x:x + w]
                        resize = cv2.resize(roi_gray, (244, 244))
                        em = -1
                        name_file = list_archivos[i][len(list_archivos[i]) - 21:len(list_archivos[i])]
                        if label == 1: #Furia
                            em = 0
                        elif label == 2: #Desprecio
                            em = 7
                        elif label == 3: #Disgusto
                            em = 1
                        elif label == 4: #Miedo
                            em = 2
                        elif label == 5: #Felicidad
                            em = 3
                        elif label == 6: #Tristeza
                            em = 4
                        elif label == 7: #Sorpresa
                            em = 5

                        cv2.imwrite(path_origin + path_save + str(em) + "_" + name_file, resize)
                        print("[" + root + "] - Otros " + name_file)
                        nro_emociones[label] = nro_emociones[label] + 1
    print(nro_emociones)



