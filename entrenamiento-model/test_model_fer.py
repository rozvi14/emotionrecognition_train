import matplotlib.pyplot as plt
import keras
from keras.models import load_model
import numpy as np

num_classes = 7
model = load_model('facial_expression_model_weights.h5')


def emotion_analysis(emotions,index):
    objects = ('Furia', 'Disgusto', 'Miedo', 'Felicidad', 'Tristeza', 'Sorpresa', 'Neutral')
    y_pos = np.arange(len(objects))

    plt.bar(y_pos, emotions, align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Porcentaje')
    plt.title('Emocion')
    plt.savefig("./imagenes/emotion_analysys_"+str(index)+".png")
    plt.show()

with open("./data/fer2013/fer2013.csv") as f:
    content = f.readlines()

lines = np.array(content)

num_of_instances = lines.size
print("Nro de imagenes: ", num_of_instances)
print("Tamaño de imagen: ", len(lines[1].split(",")[1].split(" ")))


x_train, y_train, x_test, y_test = [], [], [], []


for i in range(1, num_of_instances):
    try:
        emotion, img, usage = lines[i].split(",")

        val = img.split(" ")

        pixels = np.array(val, 'float32')

        emotion = keras.utils.to_categorical(emotion, num_classes)

        if 'Training' in usage:
            y_train.append(emotion)
            x_train.append(pixels)
        elif 'PublicTest' in usage:
            y_test.append(emotion)
            x_test.append(pixels)
    except:
        print("", end="")


x_train = np.array(x_train, 'float32')
y_train = np.array(y_train, 'float32')
x_test = np.array(x_test, 'float32')
y_test = np.array(y_test, 'float32')

x_train /= 255
x_test /= 255


x_train = x_train.reshape(x_train.shape[0], 48, 48, 1)
x_train = x_train.astype('float32')
print(x_train.shape)
x_test = x_test.reshape(x_test.shape[0], 48, 48, 1)
x_test = x_test.astype('float32')


print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

predictions = model.predict(x_test)

index = 0
for i in predictions:
    if index < 30 and index >= 20:
        # print(i) #predicted scores
        # print(y_test[index]) #actual scores

        testing_img = np.array(x_test[index], 'float32')
        testing_img = testing_img.reshape([48, 48]);

        plt.gray()
        plt.imshow(testing_img)
        plt.savefig("./imagenes/image_" + str(index) + ".png")
        plt.show()

        print(i)

        emotion_analysis(i,index)
        print("----------------------------------------------")
    index = index + 1
