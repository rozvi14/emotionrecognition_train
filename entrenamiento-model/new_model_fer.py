from idlelib.idle_test.test_iomenu import S
import os
import tensorflow as tf
import keras
import numpy as np
import matplotlib.pyplot as plt
from pandas import DataFrame, read_csv

from entrenar_model import entrenar_model, PATH_CSV_RESULT, columns
import Modelos
nro_model = 3
num_classes = 6

batch_size = 256
epochs = 100

model, NAME_MODEL = Modelos.generar_model(num_classes, nro_model)
MODEL = NAME_MODEL+"_6emo"
name_bd = "FER"
tipo_norm = "Max"

if __name__ == '__main__':
    config = tf.ConfigProto( device_count = {'GPU': 1 , 'CPU': 56} ) #max: 1 gpu, 56 cpu
    sess = tf.Session(config=config)
    keras.backend.set_session(sess)

    if os.path.isfile(PATH_CSV_RESULT):
        csvResult = read_csv(PATH_CSV_RESULT, index_col=0)
    else:
        csvResult = DataFrame(columns=columns)


    with open("./data/fer2013/fer2013.csv") as f:
        content = f.readlines()

    lines = np.array(content)

    num_of_instances = lines.size
    print("Nro de imagenes: ", num_of_instances)
    print("Tamaño de imagen: ", len(lines[1].split(",")[1].split(" ")))


    x_train, y_train, x_test, y_test = [], [], [], []


    for i in range(1, num_of_instances):
        try:
            label, img, usage = lines[i].split(",")

            val = img.split(" ")

            pixels = np.array(val, 'float32')
            label = int(label)
            if label < 7 and label != 1:
                if label > 1:
                    label = label - 1
                emotion = keras.utils.to_categorical(label, num_classes)

                if 'Training' in usage:
                    y_train.append(emotion)
                    x_train.append(pixels)
                elif 'PublicTest' in usage:
                    y_test.append(emotion)
                    x_test.append(pixels)
        except:
            print("", end="")


    x_train = np.array(x_train, 'float32')
    y_train = np.array(y_train, 'float32')
    x_test = np.array(x_test, 'float32')
    y_test = np.array(y_test, 'float32')

    x_train /= 255
    x_test /= 255


    x_train = x_train.reshape(x_train.shape[0], 48, 48, 1)
    x_train = x_train.astype('float32')
    print(x_train.shape)
    x_test = x_test.reshape(x_test.shape[0], 48, 48, 1)
    x_test = x_test.astype('float32')


    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    NOMBRE_MODEL = MODEL + "_" + name_bd + "_" + tipo_norm + "_E" + str(epochs) + "_B" + str(batch_size)


    model, history, score = entrenar_model(x_train,y_train,x_test,y_test,epochs,batch_size, NOMBRE_MODEL)

    csvResult = csvResult.append({
        'model': MODEL, 'BD': name_bd, 'Norm': tipo_norm, 'Epochs': epochs, 'Batchs': batch_size, 'Accu': score[1],
        'Loss': score[0]
    }, ignore_index=True)
    print("=========== GUARDANDO RESULTADOS ===============")
    csvResult.to_csv('./resultados_time.csv')


    # ------------------------------
    # Prediccion por emocion
def emotion_analysis(emotions,index):
    objects = ('Furia', 'Disgusto', 'Miedo', 'Felicidad', 'Tristeza', 'Sorpresa', 'Neutral')
    y_pos = np.arange(len(objects))

    plt.bar(y_pos, emotions, align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Porcentaje')
    plt.title('Emocion')
    plt.savefig("./imagenes/emotion_analysys_"+str(index)+".png")
    plt.show()


    # ------------------------------

    monitor_testset_results = False

    if monitor_testset_results == True:

        predictions = model.predict(x_test)

        index = 0
        for i in predictions:
            if index < 30 and index >= 20:
                # print(i) #predicted scores
                # print(y_test[index]) #actual scores

                testing_img = np.array(x_test[index], 'float32')
                testing_img = testing_img.reshape([48, 48]);

                plt.gray()
                plt.imshow(testing_img)
                plt.show()

                print(i)

                emotion_analysis(i,index)
                print("----------------------------------------------")
            index = index + 1



