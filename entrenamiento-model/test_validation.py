from keras.models import load_model
from scipy import misc
import cv2
import numpy as np
import pandas as pd
import os
import datetime
from sklearn.preprocessing import normalize
from entrenar_model import  leer_imagenes, URL_IMAGES

model = load_model('./models/Otros/facial_expression_model_weights.h5')
emotions = ('Furia', 'Disgusto', 'Miedo', 'Felicidad', 'Tristeza', 'Sorpresa', 'Neutral')

if __name__ == '__main__':
    prueba_unitaria = False
    prueba_set = True

    if prueba_set:
        columns = ['model', 'nro', 'tiempo']
        PATH_CSV_TEST_TIME = './resultados_time.csv'
        if os.path.isfile(PATH_CSV_TEST_TIME):
            csvResult = pd.read_csv(PATH_CSV_TEST_TIME, index_col=0)
        else:
            csvResult = pd.DataFrame(columns=columns)

        time = []
        index_db = 1
        index_norm = 0
        #URL_IMAGEN = "./data/extended-cohn-kanade-images/cohn-kanade-haar"
        X, y = leer_imagenes(URL_IMAGES[index_db], index_norm)
        print("FIN LECTURA IMG")
        for i in range(0, len(X)):
            print("INICIO IMAGEN: "+str(i))

            data = X[i]
            data = np.expand_dims(data, axis=0)
            time_init = datetime.datetime.now()
            y_predict = model.predict(data)
            time_fin = datetime.datetime.now()
            csvResult = csvResult.append({
                'model': 'Propuesto 1',
                'nro': i,
                'tiempo': (time_fin-time_init).microseconds/1000
            }, ignore_index=True)
            print("FIN IMAGEN: " + str(i))
        print("=========== GUARDANDO RESULTADOS ===============")
        csvResult.to_csv(PATH_CSV_TEST_TIME)

        #score = model.evaluate(X, y)
        #print('Test loss:', score[0])
        #print('Test accuracy:', 100 * score[1])


    if prueba_unitaria:
        frame = cv2.imread('./data/extended-cohn-kanade-images/cohn-kanade-haar/5_S069_002_00000014.png')
        detected_face = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        detected_face = cv2.resize(detected_face, (48, 48))
        data = np.array(detected_face, 'float32')
        data = normalize(data, norm='l2')
        x = data.reshape(48, 48, 1)
        x = np.expand_dims(x, axis=0)
        predictions = model.predict(x)
        print(predictions)
        max_index = int(np.argmax(predictions[0]))
        emotion = emotions[max_index]
        print(max_index)
        print(emotion)
        cv2.imshow('frame', detected_face)
        cv2.waitKey()
        cv2.destroyAllWindows()

