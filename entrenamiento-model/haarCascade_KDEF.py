import os
import cv2
import glob

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def get_emotion(label):
    em = -1
    if (label == 'AN'):
        em = 0
    elif (label == 'DI'):
        em = 1
    elif (label == 'AF'):
        em = 2
    elif(label=='HA'):
        em = 3
    elif(label=='SA'):
        em = 4
    elif (label == 'SU'):
        em = 5
    elif (label == 'NE'):
        em = 6

    return em

if __name__ == '__main__':
    path_img = './data/KDEF_and_AKDEF/KDEF_and_AKDEF/KDEF'
    path_save = './data/KDEF_and_AKDEF/KDEF_and_AKDEF/KDEFHaar/'

    lstDir = os.walk(path_img)
    nro_emociones = [0, 0, 0, 0, 0, 0, 0, 0]

    for root, dirs, files in lstDir:
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
            if (extension == ".JPG"):
                if nombreFichero[len(nombreFichero)-1:len(nombreFichero)] == 'S':
                    em = get_emotion(nombreFichero[4:6])

                    roi_gray = []
                    img = cv2.imread(root + "/" + fichero)
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
                    for (x, y, w, h) in faces:
                        roi_gray = img[y:y + h, x:x + w]
                    if roi_gray.any():
                        resize = cv2.resize(roi_gray, (244, 244))
                        cv2.imwrite(path_save + str(em) + "_" + nombreFichero+".png", resize)
                        print("[" + root + "] - "+str(nombreFichero[4:6]) + nombreFichero)
                        nro_emociones[em] = nro_emociones[em] + 1

    print(nro_emociones)