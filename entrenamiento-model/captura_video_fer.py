import cv2
import numpy as np
from keras.models import load_model, model_from_json
from keras.preprocessing import image
from sklearn.preprocessing import normalize

cap = cv2.VideoCapture(0)
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#model = model_from_json(open("facial_expression_model_structure.json", "r").read())
#model = load_model('./models/OWN4_6emo_All_Max_E50_B17-model.h5')
model = load_model('./models/OWNRETRAIN2_6emo_Server_Max_E100_B6-model.h5')
type_norm = 0
#emotions = ('Furia', 'Disgusto', 'Miedo', 'Felicidad', 'Tristeza', 'Sorpresa', 'Neutral')
emotions = ('Furia', 'Miedo', 'Felicidad', 'Tristeza', 'Sorpresa', 'Neutral')


if __name__ == '__main__':
    print("[INICIO]")
    if(cap.isOpened() == False):
        print("No se puede abrir la camara")

    else:

        while(True):
            ret, frame = cap.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            color_gray = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
            dim_img = frame.shape

            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                detected_face = frame[int(y):int(y + h), int(x):int(x + w)]
                #cv2.imwrite('./emotionrecognition/test_2.png',detected_face)
                detected_face = cv2.cvtColor(detected_face, cv2.COLOR_BGR2GRAY)
                detected_face = cv2.resize(detected_face, (48, 48))
                data = np.array(detected_face, 'float32')
                if type_norm == 0:
                    data /= 255
                elif type_norm == 1:
                    data = normalize(data, norm='l2')
                img_pixels = image.img_to_array(data)

                img_pixels = np.expand_dims(img_pixels, axis=0)

                predictions = model.predict(img_pixels)

                max_index = int(np.argmax(predictions[0]))

                emotion = emotions[max_index]

                cv2.putText(frame, emotion, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 2)



            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()

