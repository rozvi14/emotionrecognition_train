import cv2
import numpy as np
from keras.models import load_model

cap = cv2.VideoCapture(0)
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
model=load_model('incprestv2_model_5e.h5')
def identify_emotion(img):
    data = cv2.resize(img, (244, 244))
    x = np.array(data, 'float32')
    x /= 255
    x = np.expand_dims(x, axis=0)
    print("[INICIO Prediccion]")
    y_predict = model.predict(x)
    ind = -1
    mayor = 0
    for i in range(0, 6):
        tmp = y_predict[0, i]
        if tmp >= mayor:
            mayor = tmp
            ind = i
    if ind == 0:
        emocion = "FELICIDAD"
    elif ind == 1:
        emocion = "TRISTEZA"
    elif ind == 2:
        emocion = "IRA"
    elif ind == 3:
        emocion = "MIEDO"
    elif ind == 4:
        emocion = "DISGUSTO"
    elif ind == 5:
        emocion = "SORPRESA"
    else:
        emocion = "NEUTRAL"
    print(y_predict)
    print("[FIN Prediccion]")
    return emocion, mayor


if __name__ == '__main__':
    print("[INICIO]")
    if(cap.isOpened() == False):
        print("No se puede abrir la camara")

    while(True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        color_gray = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        dim_img = frame.shape
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = color_gray[y:y + h, x:x + w]
            emocion, ratio = identify_emotion(roi_gray)
            accur_predic = str(round(ratio * 100, 2)) + "%"
            print(emocion +": "+ accur_predic)
            cv2.putText(frame, emocion, (10, dim_img[0]), cv2.FONT_HERSHEY_SIMPLEX, 4, (255, 255, 255), 8,
                        cv2.LINE_AA)


        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

