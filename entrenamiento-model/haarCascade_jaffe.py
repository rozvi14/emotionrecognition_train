import numpy as np
import cv2
import os

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

def recortarImg(img_path,img_name, path_save):
    img=cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        roi_gray = img[y:y + h, x:x + w]

    resize = cv2.resize(roi_gray, (244, 244))
    label = img_name[3:5]
    print(label)
    em = -1
    if (label == 'AN'):
        em = 0
    elif (label == 'DI'):
        em = 1
    elif (label == 'FE'):
        em = 2
    elif(label=='HA'):
        em = 3
    elif(label=='SA'):
        em = 4
    elif (label == 'SU'):
        em = 5
    else: #neutral
        em = 6

    cv2.imwrite(path_save + str(em) + "_" + img_name + '.png', resize)

    #print(img_name)
    #cv2.imshow('img', roi_gray)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()



#recortarImg('.\jaffeimages\jaffe\YM.HA2.53.tiff')


#RECORRER LA CARPETA
#path="jaffeimages\\jaffe_haar"
if __name__ == '__main__':

    path = './data/jaffeimages/jaffe/' #CARPETA DONDE SE ENCUENTRA CARPETA DE IMAGENES
    path_save = './data/jaffeimages/jaffe_haar2/' #CARPETA DONDE GUARDAR IMAGENES
    lstFiles = []
    lstDir =os.walk(path)
    print("INICIO")
    print(lstDir)
    i=1
    for root, dirs, files in lstDir:
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
            if (extension == ".tiff" or extension==".png"):
                print("INICIO IMG "+str(i)+" "+nombreFichero)
                recortarImg(root+fichero, nombreFichero, path_save)
                print(f"FIN    {i}")
                i=i+1

