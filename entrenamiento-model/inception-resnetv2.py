from keras import Model
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.vgg16 import VGG16

import os
import matplotlib.pyplot as plt
import numpy as np
import cv2
import pandas as pd
import datetime
from keras.layers import Dense, Input
from keras.utils import np_utils, plot_model
from sklearn.model_selection import train_test_split
from keras.models import load_model


def leer_imagenes():
    path = "./data/jaffeimages/jaffe_haar"
    X = []
    y = []
    lstFiles = []
    lstDir = os.walk(path)
    i = 1
    for root, dirs, files in lstDir:
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
            if (extension == ".png"):
                ruta_img = root + '\\' + nombreFichero + extension
                label = int(nombreFichero[0:1])-1
                data = cv2.imread(ruta_img)
                if (data.any()):
                    # data=np.array(data)

                    X.append(data)
                    y.append(label)
    X = np.array(X, 'float32')
    X /= 255
    y = np.array(y)
    return X, y


if __name__ == '__main__':
    #os.environ['THEANO_FLAGS'] = 'device=gpu'
    #matplotlib.style.use('ggplot')

    print("Lectura de data")
    X, y = leer_imagenes()
    print("Fin lectura de data")

    train = False
    test = True

    if test:
        model = load_model('./models/Otros/incprestv2_model_5e.h5')

        columns = ['model', 'nro', 'tiempo']
        PATH_CSV_TEST_TIME = './resultados_time.csv'
        if os.path.isfile(PATH_CSV_TEST_TIME):
            csvResult = pd.read_csv(PATH_CSV_TEST_TIME, index_col=0)
        else:
            csvResult = pd.DataFrame(columns=columns)

        for i in range(0, len(X)):
            print("INICIO IMAGEN: "+str(i))
            time_init = datetime.datetime.now()
            data = X[i]
            data = np.expand_dims(data, axis=0)
            y_predict = model.predict(data)
            csvResult = csvResult.append({
                'model': 'Inception',
                'nro': i,
                'tiempo': (datetime.datetime.now()-time_init).microseconds/1000
            }, ignore_index=True)
            print("FIN IMAGEN: " + str(i))
        print("=========== GUARDANDO RESULTADOS ===============")
        csvResult.to_csv(PATH_CSV_TEST_TIME)


    if train:

        validation_size = 0.20
        seed = 10
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=validation_size, random_state=seed)
        num_train, height, width, depth = X_train.shape
        num_test = X_test.shape[0]
        num_classes = np.unique(y_train).shape[0]



        Y_train = np_utils.to_categorical(y_train, num_classes)
        Y_test = np_utils.to_categorical(y_test, num_classes)
        Y=np_utils.to_categorical(y, num_classes)

        batch_size = 16  # número de ejemplos de entrenamiento
        num_epochs = 10 # número de veces que el algoritmo se repetirá

        input_model = Input(shape=(244,244,3))
        #base_model = InceptionResNetV2(include_top=False,input_shape=(244, 244, 3),weights='imagenet', input_tensor=input_model, pooling='avg',classes=num_classes)
        #base_model = InceptionResNetV2(include_top=False, input_shape=(244, 244, 3), pooling='avg')

        base_model = VGG16(include_top=False,input_shape=())

        out = Dense(num_classes, activation='softmax')(base_model.output)

        model = Model(base_model.inputs, out)

        print("COMPILADO MODELO")
        model.compile(loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])  # reporting the accuracy
        print("ENTRANAMIENTO MODELO")

        history = model.fit(X, Y,  # Train the model using the training set...
                                  batch_size=batch_size, epochs=num_epochs,
                                  verbose=1
                                  ,validation_split=0.2
                                  )



        score = model.evaluate(X_test, Y_test, verbose=1)  # Evaluate the trained model on the test set!
        print(f"RATIO: {score[1]*100} %")
        #plot_model(model_details)
        model.save('incprestv2_model_5e.h5')

        # summarize history for accuracy
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('Exactitud del Modelo')
        plt.ylabel('Exactitud')
        plt.xlabel('Iteración')
        plt.legend(['Entranamiento','Prueba'], loc='upper left')
        plt.savefig('resnet_acc_fer.png')
        plt.show()

        # summarize history for loss
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Perdida del modelo')
        plt.ylabel('Perdida')
        plt.xlabel('Iteración')
        plt.legend(['Entranamiento','Prueba'], loc='upper left')
        plt.savefig('resnet_loss_2.png')
        plt.show()


